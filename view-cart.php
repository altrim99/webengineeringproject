<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | View Cart</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/view-cart.css" type="text/css">
</head>
<body>
    <?php 
    require "header.php";
    ?>
    <main>
        <section class="section-one">
            <article>
                <h2>Shop</h2>
                <div>
                    <a href="index.php">Home</a>
                    <span class="after-icon">></span>
                    <span class="cart-span">Cart</span>
                </div>
            </article>
        </section>
        <section>
            <div class="cart-details">
                <h1>Your cart is</h1>
                <div class="empty-cart">
                    <h1>Currently</h1>
                    <h1>Empty</h1>
                </div>
            </div>
        </section>
    </main>
    <?php 
    require "footer.php";
    ?>
    <script src="shared.js"></script>
</body>
</html>
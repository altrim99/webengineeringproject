<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | About Us</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/aboutus.css" type="text/css">
</head>
<body>
    <div class="backdrop"></div>
    <header class="main-header">
        <div>
            <a href="../index.php" class="main-header__logo">
                <img src="../images/Jevelin_logo_dark.png" alt="Jevelin - your online shop for apple watch">
            </a>
        </div>
        <nav class="main-nav">
            <ul class="main-nav__items">
                <li class="main-nav__item">
                    <a href="../index.php">Home</a>
                </li>
                <li class="main-nav__item">
                    <a href="../shop.php">Shop</a>
                </li>
                <li class="main-nav__item">
                    <a href="#">Reviews</a>
                </li>
                <li class="main-nav__item">
                    <a href="aboutus.html" class="active">About Us</a>
                </li>
                <li class="main-nav__item">
                    <a href="../contact.php">Contact</a>
                </li>
            </ul>
        </nav>
        <button class="toggle-button">
            <span class="toggle-button__bar"></span>
            <span class="toggle-button__bar"></span>
            <span class="toggle-button__bar"></span>
        </button>
        <nav class="right-nav">
            <ul class="right-nav__items">
                <li class="right-nav__item">
                <?php 
                if (isset($_SESSION['userNameId'])) {
                    echo '<a href="view-cart.php" id="cart-icon-1" class="cart-icon">
                        <i class="fa fa-shopping-cart fa-lg"><span id="cart-count" class="badge">0</span></i>
                        </a>
                    <form class="logout-style" style="margin: 0; padding: 0; display: inline;" action="../includes/logout.php" method="post">
                        <button style="border: none;
                        background: #6394F8;
                        color: white;
                        cursor: pointer;
                        width: 80px;
                        height: 25px;
                        border-radius: 15px; "type="submit" name="logout-submit">Logout</button>
                    </form>';
                } else {
                    echo '<a href="../login.php" id="login" class="login-style">Log in</a>
                    |
                    <a href="../register.php" id="signup" class="signup-style">Sign up</a>';
                }
                ?>
                </li>
            </ul>
        </nav>
    </header>
    <nav class="mobile-nav">
        <ul class="mobile-nav__items">
            <li class="mobile-nav__item">
                <a href="../index.php">Home</a>
            </li>
            <li class="mobile-nav__item">
                <a href="../shop.php">Shop</a>
            </li>
            <li class="mobile-nav__item">
                <a href="#">Reviews</a>
            </li>
            <li class="mobile-nav__item">
                <a href="about.html">About Us</a>
            </li>
            <li class="mobile-nav__item">
                <a href="../contact.php">Contact</a>
            </li>
            <li class="mobile-nav__item">
                <a href="../view-cart.php">Cart</a>
            </li>
            <li class="mobile-nav__item">
                <a href="../login.php">Log in</a>
            </li>
            <li class="mobile-nav__item">
                <a href="../register.php">Sign Up</a>
            </li>
        </ul>
    </nav>
    <main>
        <section>
            <section class="section-one">
                <article>
                    <h2>Hear our <span style="color: #7d1eff;">story</span></h2>
                    <div>
                        <a href="../index.php">Home</a>
                        <span class="after-icon">></span>
                        <span class="about-span">About Us</span>
                    </div>
                </article>
            </section>
        </section>
        <section class="section-wrapper">
            <article class="first__article">
                <h1>- Our Mission -</h1>
                <p class="paragraf__mission">To provide the most fashionable apple watch store</p>
                <div class="about__desc">
                    <h1>About Us</h1>
                    <p>Welcome to Jevelin - apple watch store, your number one source for all apple watches. 
                        We're dedicated to providing you the very best of smart watch, with an emphasis on battery life, 
                        increased storage, display surface. Founded in 2018, Jevelin - apple watch store has come a long way from its beginnings in Pristina, Kosovo. 
                        We now serve customers all over country, and are thrilled that we're able to turn our passion into our own website. We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, 
                        please don't hesitate to contact us.
                    </p>
                </div>
            </article>
            <article class="second__article">
                <h1>Leadership</h1>
                <div class="second__article--CEOs">
                    <div class="leader__desc-one">
                        <div class="leader__image"></div>
                        <h1>Name&Surname</h1>
                        <h2>Co Founder & CEO</h2>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima, quisquam! 
                            Aliquam labore perferendis minima deleniti reiciendis. Amet quas cupiditate 
                            possimus similique. Voluptatibus doloremque facilis sint suscipit obcaecati alias culpa odit.
                        </p>
                    </div>
                    <div class="leader__desc-two">
                        <div class="leader__image"></div>
                        <h1>Name&Surname</h1>
                        <h2>Co Founder & CEO</h2>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima, quisquam! 
                            Aliquam labore perferendis minima deleniti reiciendis. Amet quas cupiditate 
                            possimus similique. Voluptatibus doloremque facilis sint suscipit obcaecati alias culpa odit.
                        </p>
                    </div>
                </div>
            </article>
        </section>
    </main>
    <footer>
        <nav class="main-footer">
            <ul class="main-footer__left-block">
                <img src="../images/Jevelin_logo_light.png">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit Dolorum.</p>
                <div class="main-footer__left-icons">
                    <i class="fab fa-cc-visa fa-lg"></i>
                    <i class="fab fa-cc-mastercard fa-lg"></i>
                    <i class="fab fa-cc-amex fa-lg"></i>
                    <i class="fab fa-cc-paypal fa-lg"></i>
                    <i class="fab fa-cc-jcb fa-lg"></i>
                </div>
                <li><i class="fa fa-lock lock-icon"></i> Secure online payment</li>
            </ul>
            <ul class="main-footer__middle-block">
                <h1>Best Experience</h1>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Shipment</a></li>
                <li><a href="#">Returns</a></li>
                <li><a href="#">Gift Carts</a></li>
                <li><a href="#">Policies</a></li>
            </ul>
            <ul class="main-footer__right-block">
                <h1>Company</h2>
                <li><a href="#">About us</a></li>
                <li><a href="#">Press</a></li>
                <li><a href="#">Offices</a></li>
                <li><a href="#">Affiliates</a></li>
                <li><a href="#">Giveaway</a></li>
            </ul>
        </nav>
        <nav class="lowest-footer">
            <div class="lowest-footer__paragraf">
                <p>Copyright &copy; Jevelin </p>
            </div>
        </nav>
    </footer>
    <script src="../shared.js"></script>
</body>
</html>
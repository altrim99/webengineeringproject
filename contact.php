<?php
require 'controllers/ContactController.php';
$contact = new ContactController;

if (isset($_POST['contact-submit'])) {
    $contact->store($_POST);
}

session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Contact</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/contact.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section>
            <section class="section-one">
                <article>
                    <h2>We value our customers</h2>
                    <div>
                        <a href="index.php">Home</a>
                        <span class="after-icon">></span>
                        <span class="contact-span">Contact</span>
                    </div>
                </article>
            </section>
        </section>
        <section class="main__section">
            <h1 class="contact-title">Don't hesitate to <span style="color: #7d1eff;">Contact Us</span></h1>
            <?php
            if (isset($_GET['error'])) {
                if ($_GET['error'] == 'emptyfields') {
                    echo '<p id="error">Please fill in all fields!</p>';
                } elseif ($_GET['error'] == 'invalidfirstnamelastname') {
                    echo '<p id="error">First Name or Last Name is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidmessage') {
                    echo '<p id="error">Invalid message!</p>';
                } elseif ($_GET['error'] == 'invalidemail') {
                    echo '<p id="error">Email is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidsubject') {
                    echo '<p id="error">Subject is invalid!</p>';
                }
            }
            ?>
            <!-- <p id="error"></p> -->
            <form action="" class="contact-form" method="post">
                <input type="text" name="name" placeholder="Name">
                <input type="text" name="surname" placeholder="Surname">
                <input type="email" name="email" placeholder="Email">
                <select name="city">
                    <option value="" selected="selected" class="grey_color">City</option>
                    <option value="Peje">Peje</option>
                    <option value="Prishtine">Prishtine</option>
                    <option value="Prizren">Prizren</option>
                    <option value="Mitrovice">Mitrovice</option>
                    <option value="Ferizaj">Ferizaj</option>
                    <option value="Gjilan">Gjilan</option>
                    <option value="Other">Other</option>
                </select>
                <input type="text" placeholder="Subject" name="subject">
                <textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
                <label for="agree-terms" class="agree-terms">
                    <input type="checkbox" id="agree-terms" name="agree-terms">
                    Agree to <a href="#">Terms &amp; Conditions</a>
                </label>
                <button type="submit" name="contact-submit">Submit</button>
            </form>
        </section>
    </main>
    <?php
    require "footer.php";
    ?>
    <script src="shared.js"></script>
    <!-- <script src="js/contact.js"></script> -->
</body>

</html>
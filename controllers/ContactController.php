<?php

require 'database/Database.php';

class ContactController
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $query = $this->db->pdo->query('SELECT * FROM contact');

        return $query->fetchAll();
    }

    public function store($request)
    {
        $firstNameUser = $request['name'];
        $lastNameUser = $request['surname'];
        $emailUser = $request['email'];
        $cityUser = $request['city'];
        $subject = $request['subject'];
        $message = $request['message'];

        if (
            empty($firstNameUser) || empty($lastNameUser) || empty($emailUser) || empty($cityUser) || empty($subject)
            || empty($message)
        ) {
            header("Location: ./contact.php?error=emptyfields");
            exit();
        } elseif ((!preg_match("/^[A-Za-z]*$/", $firstNameUser) && !preg_match("/^[A-Za-z]*$/", $lastNameUser))
            || (!preg_match("/^[A-Za-z]*$/", $firstNameUser) || !preg_match("/^[A-Za-z]*$/", $lastNameUser))
        ) {
            header("Location: ./contact.php?error=invalidfirstnamelastname");
            exit();
        } elseif (!preg_match("/[^A-Za-z0-9 .'?!,@$#-_]*$/", $message)) {
            header("Location: ./contact.php?error=invalidmessage&email=" . $emailUser);
            exit();
        } elseif (!filter_var($emailUser, FILTER_VALIDATE_EMAIL)) {
            header("Location: ./contact.php?error=invalidemail");
            exit();
        } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $subject)) {
            header("Location: ./contact.php?error=invalidsubject");
        } else {
            $query = $this->db->pdo->prepare('INSERT INTO contact (name, surname, email, city, subject, message) VALUES (:firstName, :lastName, :email, :city, :subject, :message)');
            $query->bindParam(':firstName', $firstNameUser);
            $query->bindParam(':lastName', $lastNameUser);
            $query->bindParam(':email', $emailUser);
            $query->bindParam(':city', $cityUser);
            $query->bindParam(':subject', $subject);
            $query->bindParam(':message', $message);
            $query->execute();

            $to = 'altrim.havolli@gmail.com';
            $headers = 'From: ' . $emailUser;
            $txt = "You have received an e-mail from " . $firstNameUser . " " . $lastNameUser . ".\n\n" . $message;

            mail($to, $subject, $headers, $txt);

            return header('Location: ./contact.php?emailsend=success');
        }
    }

    public function edit($id)
    {
        $query = $this->db->pdo->prepare('SELECT * FROM contact WHERE id = :id');
        $query->execute(['id' => $id]);

        return $query->fetch();
    }

    public function update($id, $request)
    {
        $query = $this->db->pdo->prepare(
            'UPDATE contact SET name = :name, surname = :surname, email = :email, city = :city, subject = :subject, message = :message WHERE id = :id'
        );
        $query->execute([
            'name' => $request['firstname'],
            'surname' => $request['lastname'],
            'email' => $request['email'],
            'city' => $request['city'],
            'subject' => $request['subject'],
            'message' => $request['message'],
            'id' => $id
        ]);

        return header('Location: ../admin/contact.php?update=success');
    }

    public function destroy($id)
    {
        $query = $this->db->pdo->prepare('DELETE FROM contact WHERE id = :id');
        $query->execute(['id' => $id]);

        return header('Location: ../admin/contact.php?destroy=success');
    }
}

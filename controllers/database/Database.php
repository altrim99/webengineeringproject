<?php

class Database
{
    public $pdo;

    public function __construct()
    {
        try {
            $conn = new PDO('mysql:host=localhost;dbname=projectdbtest', 'root', 'root');
            $this->pdo = $conn;
        } catch (PDOException $th) {
            die('DIE-> Connection failed: '. $th->getMessage());
        }
    }
}
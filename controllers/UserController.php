<?php

require 'database/Database.php';

class UserController
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $query = $this->db->pdo->query('SELECT * FROM users');

        return $query->fetchAll();
    }

    public function store($request)
    {
        $firstNameUser = $request['firstname'];
        $lastNameUser = $request['lastname'];
        $phoneNoUser = $request['phone'];
        $addressUser = $request['address'];
        $usernameUser = $request['username'];
        $emailUser = $request['email'];
        $passwordUser = $request['password'];
        $user_type = $request['role'];

        if (
            empty($firstNameUser) || empty($lastNameUser) || empty($phoneNoUser) || empty($addressUser) || empty($usernameUser)
            || empty($emailUser) || empty($passwordUser) || empty($user_type)
        ) {
            header("Location: ./create-user.php?error=emptyfields");
            exit();
        } elseif ((!preg_match("/^[A-Za-z]*$/", $firstNameUser) && !preg_match("/^[A-Za-z]*$/", $lastNameUser))
            || (!preg_match("/^[A-Za-z]*$/", $firstNameUser) || !preg_match("/^[A-Za-z]*$/", $lastNameUser))
        ) {
            header("Location: ./create-user.php?error=invalidfirstnamelastname");
            exit();
        } elseif ((!preg_match("/^[0-9]{9}+$/", $phoneNoUser))) {
            header("Location: ./create-user.php?error=invalidphone");
            exit();
        } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $usernameUser)) {
            header("Location: ./create-user.php?error=invalidusername&email=" . $emailUser);
            exit();
        } elseif (!filter_var($emailUser, FILTER_VALIDATE_EMAIL)) {
            header("Location: ./create-user.php?error=invalidemail&uid=" . $usernameUser);
            exit();
        } else {

            $sql = $this->db->pdo->prepare('SELECT count(*) FROM users WHERE username=:username');
            $sql->bindParam(':username', $usernameUser);
            $sql->execute();

            $result = $sql->fetchColumn();
            if ($result > 0) {
                header('Location: ./create-user.php?error=usertaken&email=' . $emailUser);
            } else {
                $password = password_hash($passwordUser, PASSWORD_DEFAULT);

                $query = $this->db->pdo->prepare('INSERT INTO users (firstName, lastName, phoneNo, addressUser, username, email, passwordUser, userType) VALUES (:firstName, :lastName, :phoneNo, :addressUser, :username, :email, :passwordUser, :userType)');
                $query->bindParam(':firstName', $firstNameUser);
                $query->bindParam(':lastName', $lastNameUser);
                $query->bindParam(':phoneNo', $phoneNoUser);
                $query->bindParam(':addressUser', $addressUser);
                $query->bindParam(':username', $usernameUser);
                $query->bindParam(':email', $emailUser);
                $query->bindParam(':passwordUser', $password);
                $query->bindParam(':userType', $user_type);
                $query->execute();

                return header('Location: ./create-user.php?registration=success');
            }
        }
    }

    public function edit($id)
    {
        $query = $this->db->pdo->prepare('SELECT * FROM users WHERE id = :id');
        $query->execute(['id' => $id]);

        return $query->fetch();
    }

    public function update($id, $request)
    {
        $query = $this->db->pdo->prepare(
            'UPDATE users SET firstName = :name, lastName = :surname, phoneNo = :phone, addressUser = :address, username = :userName, email = :userEmail, userType = :usertype WHERE id = :id'
        );
        $query->execute([
            'name' => $request['firstname'],
            'surname' => $request['lastname'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'userName' => $request['username'],
            'userEmail' => $request['email'],
            'usertype' => $request['role'],
            'id' => $id
        ]);

        return header('Location: ./users.php?update=success');
    }

    public function destroy($id)
    {
        $query = $this->db->pdo->prepare('DELETE FROM users WHERE id = :id');
        $query->execute(['id' => $id]);

        return header('Location: ../admin/users.php?destroy=success');
    }
}

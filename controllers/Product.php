<?php

require 'database/Database.php';

session_start();

class Product
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function findAll()
    {
        $query = $this->db->pdo->query('SELECT * FROM products');

        return $query->fetchAll();
    }

    public function store($request)
    {

        $imageUploaded = $_FILES['imageUploader']['name'];
        $tmp_dir = $_FILES['imageUploader']['tmp_name'];

        $path = '../products/';
        $imgExt = strtolower(pathinfo($imageUploaded, PATHINFO_EXTENSION));
        $image = rand(1000, 1000000).".".$imgExt;
        move_uploaded_file($tmp_dir, $path.$image);

        $query = $this->db->pdo->prepare('INSERT INTO products (image, name, description, price, created_by) VALUES (:img, :name, :description, :price, :created_by)');
        $query->bindParam(':img', $image);
        $query->bindParam(':name', $request['name']);
        $query->bindParam(':description', $request['description']);
        $query->bindParam(':price', $request['price']);
        $query->bindParam(':created_by', $_SESSION['adminUserId']);
        if($query->execute()){
            return header('Location: ./create-product.php?register=success');
        } else {
            return header('Location: ./create-product.php?error=nodatafound');
        }
    }

    public function edit($id)
    {
        $query = $this->db->pdo->prepare('SELECT * FROM products WHERE id = :id');
        $query->execute(['id' => $id]);

        return $query->fetch();
    }

    public function update($id, $request)
    {
        $imageUploaded = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];

        $path = '../products/';
        $imgExt = strtolower(pathinfo($imageUploaded, PATHINFO_EXTENSION));
        $image = rand(1000, 1000000).".".$imgExt;
        move_uploaded_file($tmp_dir, $path.$image);

        $query = $this->db->pdo->prepare('UPDATE products SET image = :image, name = :name, description = :description, price = :price WHERE id = :id');
        $query->execute([
            ':image' => $image,
            ':name' => $request['name'],
            ':description' => $request['description'],
            ':price' => $request['price'],
            ':id' => $id
        ]);

        return header('Location: ./products.php?updated=success');
    }

    public function destroy($id)
    {
        $query = $this->db->pdo->prepare('DELETE FROM products WHERE id = :id');
        $query->execute(['id' => $id]);

        return header('Location: ./products.php?destroy=success');
    }
}

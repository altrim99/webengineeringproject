<?php

require_once 'database/Database.php';

class AuthController
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function login()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $query = $this->db->pdo->prepare('SELECT * FROM users WHERE username = :username');
        $query->bindParam(':username', $username);
        $query->execute();

        $user = $query->fetch();

        if (!$user) {
            header('Location: ./login.php?error=emptyfields');
        } else if ($user) {
            $pwdCheck = password_verify($password, $user['passwordUser']);
            if (empty($username) || empty($password)) {
                header('Location: ./login.php?error=emptyfields');
            } elseif (count($user) > 0 && $pwdCheck) {
                $usertype = $user['userType'];
                if ($usertype == 'user') {
                    session_start();
                    $_SESSION['userId'] = $user['id'];
                    $_SESSION['userNameId'] = $user['username'];
                    header("Location: ./index.php?login=success");
                }
            }
        }
    }

    public function adminLogin($request)
    {
        $query = $this->db->pdo->prepare('SELECT * FROM users WHERE username = :username');
        $query->bindParam(':username', $request['username']);
        $query->execute();

        $user = $query->fetch();

        if ($user) {
            $pwdCheck = password_verify($request['password'], $user['passwordUser']);
            if (count($user) > 0 && $pwdCheck) {
                $usertype = $user['userType'];
                if ($usertype == 'admin') {
                    session_start();
                    $_SESSION['userId'] = $user['id'];
                    $_SESSION['firstName'] = $user['firstName'];
                    $_SESSION['lastName'] = $user['lastName'];
                    $_SESSION['phone'] = $user['phoneNo'];
                    $_SESSION['address'] = $user['addressUser'];
                    $_SESSION['adminUserId'] = $user['username'];
                    $_SESSION['email'] = $user['email'];
                    header("Location: dashboard.php?login=success");
                } else {
                    header("Location: ./index.php?error=nouser");
                }
            }
        }
    }

    public function store($request)
    {
        $firstNameUser = $request['firstname'];
        $lastNameUser = $request['lastname'];
        $phoneNoUser = $request['phone'];
        $addressUser = $request['address'];
        $usernameUser = $request['username'];
        $emailUser = $request['email'];
        $passwordUser = $request['password'];
        $passRepeatUser = $request['password-repeat'];
        $user_type = 'user';

        if (
            empty($firstNameUser) || empty($lastNameUser) || empty($phoneNoUser) || empty($addressUser) || empty($usernameUser)
            || empty($emailUser) || empty($passwordUser) || empty($passRepeatUser)
        ) {
            header("Location: ./register.php?error=emptyfields");
            exit();
        } elseif ((!preg_match("/^[A-Za-z]*$/", $firstNameUser) && !preg_match("/^[A-Za-z]*$/", $lastNameUser))
            || (!preg_match("/^[A-Za-z]*$/", $firstNameUser) || !preg_match("/^[A-Za-z]*$/", $lastNameUser))
        ) {
            header("Location: ./register.php?error=invalidfirstnamelastname");
            exit();
        } elseif ((!preg_match("/^[0-9]{9}+$/", $phoneNoUser))) {
            header("Location: ./register.php?error=invalidphone");
            exit();
        } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $usernameUser)) {
            header("Location: ./register.php?error=invalidusername&email=" . $emailUser);
            exit();
        } elseif (!filter_var($emailUser, FILTER_VALIDATE_EMAIL)) {
            header("Location: ./register.php?error=invalidemail&username=" . $usernameUser);
            exit();
        } elseif ($passwordUser !== $passRepeatUser) {
            header("Location: ./register.php?error=passwordcheck&username=" . $usernameUser . "&email=" . $emailUser);
            exit();
        } else {
            $sql = $this->db->pdo->prepare('SELECT count(*) FROM users WHERE username=:username');
            $sql->bindParam(':username', $usernameUser);
            $sql->execute();

            $result = $sql->fetchColumn();
            if ($result > 0) {
                header('Location: ./register.php?error=usertaken&email='.$emailUser);
            } else {
                $password = password_hash($passwordUser, PASSWORD_DEFAULT);

                $query = $this->db->pdo->prepare('INSERT INTO users (firstName, lastName, phoneNo, addressUser, username, email, passwordUser, userType) VALUES (:firstName, :lastName, :phoneNo, :addressUser, :username, :email, :passwordUser, :userType)');
                $query->bindParam(':firstName', $firstNameUser);
                $query->bindParam(':lastName', $lastNameUser);
                $query->bindParam(':phoneNo', $phoneNoUser);
                $query->bindParam(':addressUser', $addressUser);
                $query->bindParam(':username', $usernameUser);
                $query->bindParam(':email', $emailUser);
                $query->bindParam(':passwordUser', $password);
                $query->bindParam(':userType', $user_type);
                $query->execute();

                return header('Location: ./index.php?registration=success');
            }
        }
    }
}

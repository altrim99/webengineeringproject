<?php

require 'controllers/Product.php';

$product = new Product;
$products = $product->findAll();

// session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Shop</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/shop.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="section-one">
            <article>
                <h2>Shop</h2>
                <div>
                    <a href="index.php">Home</a>
                    <span class="after-icon">></span>
                    <span class="products-span">Products</span>
                </div>
            </article>
        </section>
        <section class="section-two">
            <article class="all__products">
                <ul class="all__products-list">
                    <?php foreach ($products as $product) : ?>
                        <li>
                            <img src="products/<?php echo $product['image'] ?>" class="product-img">
                            <div class="product-details">
                                <a href="">
                                    <h2><?php echo $product['name'] ?></h2>
                                    <p><?php echo $product['description'] ?></p>
                                    <span class="price">$<?php echo $product['price'] ?></span>
                                    <?php if (isset($_SESSION['adminUserId'])) : ?>
                                        <p id="created_by" style="opacity: 0.7;">Created by: <?php echo $product['created_by'] ?></p>
                                    <?php endif; ?>
                                    <a href="view-cart.php" class="add-to-cart">
                                        <span>
                                            <i class="fa fa-shopping-cart icon-cart"></i>
                                        </span>
                                    </a>
                                </a>
                            </div>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </article>
        </section>
    </main>
    <?php
    require "footer.php";
    ?>
    <script src="shared.js"></script>
</body>

</html>
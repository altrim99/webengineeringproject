const backdrop = document.getElementsByClassName("backdrop")[0];
const modal = document.getElementsByClassName("modal")[0];
const modalNoButton = document.getElementsByClassName("modal__action--negative")[0];
const selectSeeMore = document.getElementsByClassName("button-see-more")[0];
const toggleButton = document.getElementsByClassName("toggle-button")[0];
const mobileNav = document.getElementsByClassName("mobile-nav")[0];

if(selectSeeMore) {
    selectSeeMore.addEventListener("click", function() {
        modal.classList.add("open");
        backdrop.style.display = "block";
        setTimeout(function() {
          backdrop.classList.add("open");
        }, 10);
    });
}
  
backdrop.addEventListener('click', function() {
    mobileNav.classList.remove('open');
    closeModal();
});
  
if (modalNoButton) {
    modalNoButton.addEventListener("click", closeModal);
}
  
function closeModal() {
    if (modal) {
      modal.classList.remove("open");
    }
    backdrop.classList.remove("open");
    setTimeout(function() {
      backdrop.style.display ="none";
    }, 200);
}
  
toggleButton.addEventListener("click", function() {
    mobileNav.classList.add("open");
    backdrop.style.display = "block";
    setTimeout(function() {
      backdrop.classList.add("open");
    }, 10);
});

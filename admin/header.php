<div class="backdrop"></div>
<header class="main-header">
    <div>
        <a href="index.php" class="main-header__logo">
            <img src="../images/Jevelin_logo_dark.png" alt="Jevelin - your online shop for apple watch">
        </a>
    </div>
    <h1 class="adminpanel-h1">Admin Panel</h1>
    <button class="toggle-button">
        <span class="toggle-button__bar"></span>
        <span class="toggle-button__bar"></span>
        <span class="toggle-button__bar"></span>
    </button>
    <nav class="right-nav">
        <ul class="right-nav__items">
            <li class="right-nav__item">
                <a href="../index.php" class="login-style">Frontend</a>
                |
                <form class="logout-style" style="margin: 0; padding: 0; display: inline;" action="../logout.php" method="post">
                    <button style="border: none;
                        background: #6394F8;
                        color: white;
                        cursor: pointer;
                        width: 80px;
                        height: 25px;
                        border-radius: 15px; " type="submit" name="logout-submit">Logout</button>
                </form>
            </li>
        </ul>
    </nav>
</header>
<nav class="mobile-nav">
    <ul class="mobile-nav__items">
        <li class="mobile-nav__item">
            <a href="dashboard.php">Dashboard</a>
        </li>
        <li class="mobile-nav__item">
            <a href="products.php">Products</a>
        </li>
        <li class="mobile-nav__item">
            <a href="users.php">Users</a>
        </li>
        <li class="mobile-nav__item">
            <a href="contact.php">Contact</a>
        </li>
    </ul>
</nav>
<nav class="left-nav">
    <ul class="left-nav__items">
        <li class="left-nav__item">
            <a href="dashboard.php">Dashboard</a>
        </li>
        <li class="left-nav__item">
            <a href="products.php">Products</a>
        </li>
        <li class="left-nav__item">
            <a href="users.php">Users</a>
        </li>
        <li class="left-nav__item">
            <a href="contact.php">Contact</a>
        </li>
    </ul>
</nav>
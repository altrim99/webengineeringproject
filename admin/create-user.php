<?php

require '../controllers/UserController.php';
$user = new UserController;

if (isset($_POST['register-submit'])) {
    $user->store($_POST);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Register</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Create <span style="color: #7d1eff;">Account</span></h1>
            <?php
            if (isset($_GET['registration'])) {
                if ($_GET['registration'] == 'success') {
                    echo '<p id="success-register">User added succesfuly!</p>';
                }
            } elseif (isset($_GET['error'])) {
                if ($_GET['error'] == 'nodatafound') {
                    echo '<p id="error-register">Somthing went wrong!</p>';
                } elseif ($_GET['error'] == 'emptyfields') {
                    echo '<p id="error-register">Please fill in all fields!</p>';
                } elseif ($_GET['error'] == 'invalidfirstnamelastname') {
                    echo '<p id="error-register">First Name or Last Name is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidphone') {
                    echo '<p id="error-register">Phone number is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidusername') {
                    echo '<p id="error-register">Username is invalid!</p>';
                } elseif ($_GET['error'] == 'usertaken') {
                    echo '<p id="error-register">Username is taken!</p>';
                } elseif ($_GET['error'] == 'invalidemail') {
                    echo '<p id="error-register">Email is invalid!</p>';
                }
            }
            ?>
            <form class="register-form" action="" method="post">
                <input type="text" id="first-name" name="firstname" placeholder="First name">
                <input type="text" name="lastname" id="last-name" placeholder="Last name">
                <input type="text" name="phone" id="phone" placeholder="Phone No.">
                <input type="text" name="address" id="address" placeholder="Address, House No. & Street Name">
                <input type="text" name="username" id="username" placeholder="Username">
                <input type="text" name="email" id="email" placeholder="Email">
                <input type="password" id="password" name="password" placeholder="Password">
                <select name="role">
                    <option value="" class="grey_color" selected="selected">Role</option>
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                </select>
                <button type="submit" id="button-signup" value="submit" name="register-submit">Register</button>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
<?php

require '../controllers/Product.php';

$product = new Product;

if (isset($_POST['register-submit'])) {
    $product->store($_POST);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Register</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Create <span style="color: #7d1eff;">Product</span></h1>
            <?php
            if (isset($_GET['register'])) {
                if ($_GET['register'] == 'success') {
                    echo '<p id="success-register">Product added succesfuly!</p>';
                }
            } elseif (isset($_GET['error'])) {
                if ($_GET['error'] == 'nodatafound') {
                    echo '<p id="error-register">Somthing went wrong!</p>';
                }
            }
            ?>
            <form class="register-form" action="" method="post" enctype="multipart/form-data">
                <input type="file" name="imageUploader" accept="*/image" multiple>
                <input type="text" name="name" placeholder="Name">
                <input type="text" name="description" placeholder="Description">
                <input type="text" name="price" placeholder="Price">
                <button type="submit" id="button-signup" value="submit" name="register-submit">Create</button>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
    <script src="../js/register.js"></script>
</body>

</html>
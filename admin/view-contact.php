<?php
    require '../controllers/ContactController.php';

    $contact = new ContactController;

    if(isset($_GET['id'])) {
        $cId = $_GET['id'];
    }

    $currenContact = $contact->edit($cId);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jelevin | View Contact</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>
<body>
<?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">View <span style="color: #7d1eff;">Contact</span></h1>
            <form class="register-form" action="" method="post">
                <label>Name:</label>
                <input type="text" value="<?php echo $currenContact['name']; ?>" disabled>
                <label>Surname:</label>
                <input type="text" value="<?php echo $currenContact['surname']; ?>" disabled>
                <label>Email:</label>
                <input type="text" value="<?php echo $currenContact['email']; ?>" disabled>
                <label>City:</label>
                <input type="text" value="<?php echo $currenContact['city']; ?>" disabled>
                <label>Subject:</label>
                <input type="text" value="<?php echo $currenContact['subject']; ?>" disabled>
                <label>Message:</label>
                <textarea value="" disabled><?php echo $currenContact['message']; ?></textarea>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>
</html>

<?php

require '../controllers/AuthController.php';
$user = new AuthController;

if (isset($_POST['admin-login-submit'])) {
    $user->adminLogin($_POST);
}

session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Admin Login</title>
    <link rel="stylesheet" href="../css/adminlogin.css" type="text/css">
</head>

<body>
    <main>
        <?php
        if (isset($_SESSION['adminUserId'])) {
            header("Location: dashboard.php");
        } elseif (isset($_SESSION['userNameId']) || isset($_SESSION[''])) {
            echo '<section class="main__section">
            <h1 class="login-title">Access <span style="color: #7d1eff;">Denied</span></h1>
            <form class="login-form"></form>
            <p id="error-login">Sorry!! You have no premission here.</p>
            <form class="login-form"></form>
            <a href="../index.php"><--Back To Home Page</a>
            <form class="login-form"></form>
            <form class="login-form"></form>
            </section>';
        } else {
            echo '<section class="main__section">
            <h1 class="login-title">Account <span style="color: #7d1eff;">Login</span></h1>
            <p id="error-login"></p>
            <form class="login-form" action="" method="post">
                <input type="text" id="username" name="username" placeholder="Username">
                <input type="password" id="password"  name="password" placeholder="Password">
                <button id="button-login" type="submit" value="submit" name="admin-login-submit">Login</button>
            </form>
            </section>';
        }
        ?>

    </main>
</body>

</html>
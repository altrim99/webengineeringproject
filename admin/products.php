<?php
require '../controllers/Product.php';

$product = new Product;
$products = $product->findAll();

if(!isset($_SESSION['adminUserId'])) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Admin Products Dashboard</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <div class="create-user">
            <h1>Products</h1>
            <button><a href="create-product.php">Add Product</a></button>
            <?php 
            if (isset($_GET['destroy'])) {
                if ($_GET['destroy'] == 'success') {
                    echo '<p id="success-register">Product deleted succesfuly!</p>';
                }
            }
            ?>
        </div>

        <div class="dashboard-table">
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Created By</th>
                    <th>Options</th>
                </tr>
                <?php foreach ($products as $p): ?>
                    <tr>
                        <td><a style="text-decoration:none;" title="view product" href="edit-product.php?id=<?= $p['id'] ?>"><?php echo $p['id']; ?></a></td>
                        <td><?php echo $p['image']; ?></td>
                        <td><?php echo $p['name']; ?></td>
                        <td><?php echo $p['description']; ?></td>
                        <td><?php echo $p['price']; ?></td>
                        <td><?php echo $p['created_by']; ?></td>
                        <td>
                            <a title="delete product" style="text-decoration:none; font-size: 1.9em; color: red" href="delete-product.php?id=<?= $p['id'] ?>">&times</a>
                            <a title="edit product" style="text-decoration:none; font-size: 1.9em; color: green; padding-left: 10px;" href="edit-product.php?id=<?= $p['id'] ?>"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
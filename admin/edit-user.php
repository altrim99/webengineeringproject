<?php
require '../controllers/UserController.php';

$user = new UserController;

if (isset($_GET['id'])) {
    $userId = $_GET['id'];
}

$currenUser = $user->edit($userId);

if (isset($_POST['save-submit'])) {
    $user->update($userId, $_POST);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jelevin | Edit User</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Edit <span style="color: #7d1eff;">User</span></h1>
            <form class="register-form" action="" method="post">
                <label>First Name:</label>
                <input type="text" value="<?php echo $currenUser['firstName']; ?>" name="firstname">
                <label>Last Name:</label>
                <input type="text" value="<?php echo $currenUser['lastName']; ?>" name="lastname">
                <label>Phone No:</label>
                <input type="text" value="<?php echo $currenUser['phoneNo']; ?>" name="phone">
                <label>Address:</label>
                <input type="text" value="<?php echo $currenUser['addressUser']; ?>" name="address">
                <label>Username:</label>
                <input type="text" value="<?php echo $currenUser['username']; ?>" name="username">
                <label>Email:</label>
                <input type="text" value="<?php echo $currenUser['email']; ?>" name="email">
                <label>Usertype:</label>
                <select name="role">
                    <option class="grey_color" selected="selected"><?php echo $currenUser['userType']; ?></option>
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                </select>
                <button type="submit" name="save-submit">Save</button>
            </form>
        </section>
        <script src="../shared.js"></script>
    </main>
</body>

</html>
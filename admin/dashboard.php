<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Admin Dashboard</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Your <span style="color: #7d1eff;">Profile</span></h1>
            <form class="register-form" action="" method="post">
                <label for="">First Name:</label>
                <input type="text" value="<?php echo $_SESSION['firstName']; ?>" disabled>
                <label for="">Last Name:</label>
                <input type="text" value="<?php echo $_SESSION['lastName']; ?>" disabled>
                <label for="">Phone Number:</label>
                <input type="text" value="<?php echo $_SESSION['phone']; ?>" disabled>
                <label for="">Address:</label>
                <input type="text" value="<?php echo $_SESSION['address']; ?>" disabled>
                <label for="">Username:</label>
                <input type="text" value="<?php echo $_SESSION['adminUserId']; ?>" disabled>
                <label for="">Email:</label>
                <input type="text" value="<?php echo $_SESSION['email']; ?>" disabled>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
<?php
require '../controllers/UserController.php';

$user = new UserController;

if (isset($_GET['id'])) {
    $userId = $_GET['id'];
}

$currenUser = $user->edit($userId);

if (isset($_POST['delete-submit'])) {
    $user->destroy($userId, $_POST);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jelevin | Delete User</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Delete <span style="color: #7d1eff;">User?</span></h1>
            <form class="register-form" action="" method="post">
                <input type="text" value="<?php echo $currenUser['firstName']; ?>">
                <input type="text" value="<?php echo $currenUser['lastName']; ?>">
                <input type="text" value="<?php echo $currenUser['phoneNo']; ?>">
                <input type="text" value="<?php echo $currenUser['addressUser']; ?>">
                <input type="text" value="<?php echo $currenUser['username']; ?>">
                <input type="text" value="<?php echo $currenUser['email']; ?>">
                <input type="usertype" value="<?php echo $currenUser['userType']; ?>">
                <button type="submit" id="button-signup" value="submit" name="delete-submit">Delete</button>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
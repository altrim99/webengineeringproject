<?php
require '../controllers/UserController.php';

$userController = new UserController;
$users = $userController->all();

if (isset($_SESSION['userNameId'])) {
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Admin Users Dashboard</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <div class="create-user">
            <h1>Users</h1>
            <button><a href="create-user.php">Create User</a></button>
            <?php 
            if (isset($_GET['destroy'])) {
                if ($_GET['destroy'] == 'success') {
                    echo '<p id="success-register">User deleted succesfuly!</p>';
                }
            } elseif (isset($_GET['update'])) {
                if ($_GET['update'] == 'success') {
                    echo '<p id="success-register">User updated succesfuly!</p>';
                }
            }
            ?>
        </div>

        <div style="overflow-x: auto;" class="dashboard-table">
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Options</th>
                </tr>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td><a style="text-decoration:none;" title="view user" href="edit-user.php?id=<?= $user['id'] ?>"><?php echo $user['id']; ?></a></td>
                        <td><?php echo $user['firstName']; ?></td>
                        <td><?php echo $user['lastName']; ?></td>
                        <td><?php echo $user['phoneNo']; ?></td>
                        <td><?php echo $user['addressUser']; ?></td>
                        <td><?php echo $user['username']; ?></td>
                        <td><?php echo $user['email']; ?></td>
                        <td><?php echo $user['userType']; ?></td>
                        <td><a title="delete user" style="text-decoration:none; font-size: 1.9em; color: red;" href="delete-user.php?id=<?= $user['id'] ?>">&times</a>
                            <a title="edit user" style="text-decoration:none; font-size: 1.9em; color: green; padding-left: 10px;" href="edit-user.php?id=<?= $user['id'] ?>"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
<?php
require '../controllers/ContactController.php';

$contactController = new ContactController;
$contact = $contactController->all();

if (isset($_SESSION['userNameId'])) {
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Admin Contact Dashboard</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <div class="create-user">
            <h1>Contacts</h1>
            <?php 
            if (isset($_GET['destroy'])) {
                if ($_GET['destroy'] == 'success') {
                    echo '<p id="success-register">Contact deleted succesfuly!</p>';
                }
            }
            ?>
        </div>

        <div class="dashboard-table">
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Options</th>
                </tr>
                <?php foreach ($contact as $con) : ?>
                    <tr>
                        <td><a style="text-decoration:none;" title="view contact" href="view-contact.php?id=<?= $con['id'] ?>"><?php echo $con['id']; ?></a></td>
                        <td><?php echo $con['name']; ?></td>
                        <td><?php echo $con['surname']; ?></td>
                        <td><?php echo $con['email']; ?></td>
                        <td><?php echo $con['city']; ?></td>
                        <td><?php echo $con['subject']; ?></td>
                        <td><?php echo $con['message']; ?></td>
                        <td><a title="delete contact" style="text-decoration:none; font-size: 1.9em; color: red;" href="delete-contact.php?id=<?= $con['id'] ?>">&times</a>
                            <a title="view contact" style="text-decoration:none; font-size: 1.9em; color: green; padding-left: 10px;" href="view-contact.php?id=<?= $con['id'] ?>"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </main>
    <script src="../js/contact.js"></script>
    <script src="../shared.js"></script>
</body>

</html>
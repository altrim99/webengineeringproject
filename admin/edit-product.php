<?php
require '../controllers/Product.php';

$product = new Product;

if (isset($_GET['id'])) {
    $productId = $_GET['id'];
}

$currenProduct = $product->edit($productId);

if (isset($_POST['save-submit'])) {
    $product->update($productId, $_POST);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jelevin | Edit User</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Edit <span style="color: #7d1eff;">Product</span></h1>
            <?php
            if (isset($_GET['updated'])) {
                if ($_GET['updated'] == 'success') {
                    echo '<p id="success-register">Product updated succesfuly!</p>';
                }
            }
            ?>
            <form class="register-form" action="" method="post" enctype="multipart/form-data">
                <label>Image:</label>
                <img src="../products/<?php echo $currenProduct['image'] ?>" class="product-img">
                <input type="file" value="<?php echo $currenProduct['image']; ?>" name="image" accept="*/image" multiple>
                <label>Name:</label>
                <input type="text" value="<?php echo $currenProduct['name']; ?>" name="name" placeholder="Name">
                <label>Description:</label>
                <input type="text" value="<?php echo $currenProduct['description']; ?>" name="description" placeholder="Description">
                <label>Price:</label>
                <input type="text" value="<?php echo $currenProduct['price']; ?>" name="price" placeholder="price">
                <button type="submit" id="button-signup" value="submit" name="save-submit">Save</button>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
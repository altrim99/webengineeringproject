<?php
require '../controllers/Product.php';

$product = new Product;
if (isset($_GET['id'])) {
    $pId = $_GET['id'];
}
$currenProduct = $product->edit($pId);

if (isset($_POST['delete-submit'])) {
    $product->destroy($pId, $_POST);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jelevin | Delete User</title>
    <link rel="stylesheet" href="../css/shared.css" type="text/css">
    <link rel="stylesheet" href="../css/register.css" type="text/css">
    <link rel="stylesheet" href="../css/adminheader.css" type="text/css">
    <link rel="stylesheet" href="../css/dashboard.css" type="text/css">
</head>

<body>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="main__section">
            <h1 class="register-title">Delete <span style="color: #7d1eff;">Product?</span></h1>
            <form class="register-form" action="" method="post">
                <input type="text" value="<?php echo $currenProduct['image']; ?>">
                <input type="text" value="<?php echo $currenProduct['name']; ?>">
                <input type="text" value="<?php echo $currenProduct['description']; ?>">
                <input type="text" value="<?php echo $currenProduct['price']; ?>">
                <input type="text" value="<?php echo $currenProduct['created_by']; ?>">
                <button type="submit" id="button-signup" value="submit" name="delete-submit">Delete</button>
            </form>
        </section>
    </main>
    <script src="../shared.js"></script>
</body>

</html>
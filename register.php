<?php 

require 'controllers/AuthController.php';
$user = new AuthController;

if (isset($_POST['register-submit'])) {
    $user->store($_POST);
}

session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Register</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/register.css" type="text/css">
</head>
<body>
    <?php 
    require "header.php";
    ?>
    <main>
        <section>
            <section class="section-one">
                <article>
                    <h2>Account</h2>
                    <div>
                        <a href="index.php">Home</a>
                        <span class="after-icon">></span>
                        <span class="register-span">Register</span>
                    </div>
                </article>
            </section>
        </section>
        <section class="main__section">
            <h1 class="register-title">Create <span style="color: #7d1eff;">Account</span></h1>
            <?php 
            if (isset($_GET['error'])) {
                if($_GET['error'] == 'emptyfields'){
                    echo '<p id="error-register">Please fill in all fields!</p>';
                } elseif ($_GET['error'] == 'invalidfirstnamelastname') {
                    echo '<p id="error-register">First Name or Last Name is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidphone') {
                    echo '<p id="error-register">Phone number is invalid!</p>';
                } elseif ($_GET['error'] == 'invalidusername') {
                    echo '<p id="error-register">Username is invalid!</p>';
                } elseif ($_GET['error'] == 'usertaken') {
                    echo '<p id="error-register">Username is taken!</p>';
                } elseif ($_GET['error'] == 'invalidemail') {
                    echo '<p id="error-register">Email is invalid!</p>';
                } elseif ($_GET['error'] == 'passwordcheck') {
                    echo '<p id="error-register">Password is not equals!</p>';
                }   
            }
            ?>
            <!-- <p id="error-register"></p> -->
            <form class="register-form" action="" onsubmit="return validateRegister(this)" method="post">
                <input type="text" id="first-name" name="firstname" placeholder="First name">
                <input type="text" name="lastname" id="last-name" placeholder="Last name">
                <input type="text" name="phone" id="phone" placeholder="Phone No.">
                <input type="text" name="address" id="address" placeholder="Address, House No. & Street Name">
                <input type="text" name="username" id="username" placeholder="Username">
                <input type="text" name="email" id="email" placeholder="Email">
                <input type="password" id="password"  name="password" placeholder="Password">
                <input type="password" id="password-repeat" name="password-repeat" placeholder="Retype Password">
                <button type="submit" id="button-signup" value="submit" name="register-submit">Register</button>
            </form>
        </section>
    </main>
    <?php 
    require "footer.php";
    ?>
    <script src="shared.js"></script>
    <!-- <script src="js/register.js"></script> -->
</body>
</html>
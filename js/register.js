var firstname = document.getElementById("first-name");
var lastname = document.getElementById("last-name");
var genderMale = document.querySelector("input[value=male]");
var genderFemale = document.querySelector("input[value=female]");
var phoneNo = document.getElementById("phone");
var address = document.getElementById("address");
var username = document.getElementById("username");
var email = document.getElementById("email");
var password = document.getElementById("password");
var rePassword = document.getElementById("password-repeat");
var registerForm = document.getElementById("register-form");
var registerBtn = document.getElementById("button-signup");
var errorRegister = document.getElementById("error-register");

function validateRegister(e) {
  
  errorRegister.style.display = 'block';

  if(firstname.value === '') {
    errorRegister.innerHTML = "Firstname is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(lastname.value === '') {
    errorRegister.innerHTML = "Lastname is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(genderMale.checked == false && genderFemale.checked == false) {
    errorRegister.innerHTML = "Gender is not selected!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(phoneNo.value === '') {
    errorRegister.innerHTML = "Phone No. is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(phoneNo.value.length > 9){
    errorRegister.innerHTML = "Max Phone No. is 9 characters!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(address.value === '') {
    errorRegister.innerHTML = "Address is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(username.value === '') {
    errorRegister.innerHTML = "Username is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(email.value === '') {
    errorRegister.innerHTML = "Email is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(password.value === '') {
    errorRegister.innerHTML = "Password is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(rePassword.value === '') {
    errorRegister.innerHTML = "Retype Password is empty!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(rePassword.value !== password.value) {
    errorRegister.innerHTML = "Password is not equal!";
    setTimeout(function () {
      errorRegister.style.display = 'none';
      errorRegister.innerHTML = '';
    }, 4000);
    return false;
  }
}
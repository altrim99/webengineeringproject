const emri = document.querySelector("input[name=name]");
const surname = document.querySelector("input[name=surname]");
const email = document.querySelector("input[name=email]");
let error = document.getElementById("error");
const comboBox = document.getElementById("combobox");
const subject = document.getElementById('subject');
const message = document.getElementById("message__text-area");
const agreeTerms = document.querySelector("input[name=agree-terms]");
const form = document.getElementById("form");
const btn = document.getElementById("button");

function validation(e) {

  error.style.display = "block";
  
  if(emri.value === ''){
    error.innerHTML = "Name is empty!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (surname.value === '') {
    error.innerHTML = "Surname is empty!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (email.value === '') {
    error.innerHTML = "Email is empty!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (comboBox.selectedIndex == 0) {
    error.innerHTML = "City is not selected!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (subject.value === ''){
    error.innerHTML = "Subject is empty!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (message.value === '') {
    error.innerHTML = "Message is empty!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = "";
    }, 4000);
    return false;
  }
  else if (!agreeTerms.checked){
    error.innerHTML = "You must agree to the terms!";
    setTimeout(function () {
      error.style.display = 'none';
      error.innerHTML = '';
    }, 4000);
    return false;
  }
}
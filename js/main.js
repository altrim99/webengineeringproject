// slider-gallery
let sliderIndex = 1;
showSlides(sliderIndex);

function plusSlides(n) {
  showSlides(sliderIndex += n);
}

function currentSlide(n) {
  showSlides(sliderIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");

  if(n > slides.length) {
    sliderIndex = 1;
  }

  if(n < 1) {
    sliderIndex = slides.length;
  }

  for(i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for(i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active-dot", "");
  }

  slides[sliderIndex - 1].style.display = "block";
  dots[sliderIndex-1].className += " active-dot";
}
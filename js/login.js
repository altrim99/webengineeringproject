var username = document.getElementById("username");
var password = document.getElementById("password");
var errorLogin = document.getElementById("error-login");
var loginform = document.getElementById("loginform");
var button = document.getElementById("button-login");

function validateLogin(e) {
  
  errorLogin.style.display = 'block';

  if(username.value === '') {
    errorLogin.innerHTML = "Username is empty!";
    setTimeout(function () {
      errorLogin.style.display = 'none';
      errorLogin.innerHTML = '';
    }, 4000);
    return false;
  }
  else if(password.value === '') {
    errorLogin.innerHTML = "Password is empty!";
    setTimeout(function () {
        errorLogin.style.display = 'none';
        errorLogin.innerHTML = '';
    }, 4000);
    return false;
  }
}
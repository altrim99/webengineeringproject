<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Home Page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/main.css" type="text/css">
</head>

<body>
    <div class="modal">
        <h1 class="modal__title">Proceed to Shop?</h1>
        <div class="modal__actions">
            <a href="shop.php" class="modal__action">Yes</a>
            <button class="modal__action modal__action--negative" type="button">No</button>
        </div>
    </div>
    <?php
    require "header.php";
    ?>
    <main>
        <section class="product-overview">
            <h1>Lifestyle<br>Smart <span style="color: rgb(153, 79, 255)">Watch</span></h1>
            <h3>Technology of the future</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. <br> Voluptatibus, doloremque voluptatem dolor.</p>
        </section>
        <section id="gallerys" class="gallery__container">
            <h2 class="gallery-min-title">About our watches</h2>
            <h1 class="gallery-title"><span style="color: #7d1eff">New</span>,<span style="color: #7d1eff;"> Improved</span> Design<br>and Performance</h1>
            <p class="gallery-paragraf">Lorem ipsum dolor sit, amet consectetur adipisicing elit.Magnam pariatur<br>quae quod voluptatum, unde minima.</p>
            <div class="gallery__list slider-container">
                <div class="mySlides fade">
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery1.jpg" alt="">
                        </a>
                    </article>
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery2.jpg" alt="">
                        </a>
                    </article>
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery3.jpg" alt="">
                        </a>
                    </article>
                </div>
                <div class="mySlides fade">
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery4.jpg" alt="">
                        </a>
                    </article>
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery5.jpg" alt="">
                        </a>
                    </article>
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery6.jpg" alt="">
                        </a>
                    </article>
                </div>
                <div class="mySlides fade">
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery7.jpg" alt="">
                        </a>
                    </article>
                    <article class="gallery__item">
                        <a href="#">
                            <img src="images/gallery8.jpg" alt="">
                        </a>
                    </article>
                </div>
                <div class="dots">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                </div>
                <div class="empty__div"></div>
            </div>
        </section>
        <section id="first-features">
            <div class="features-image">
                <img src="images/watch-and-iphone.jpg">
            </div>
            <div class="features__lists">
                <h4 class="features__min-title">Meet With Our</h4>
                <h4 class="feature__title">Splendid Features</h4>
                <div class="feature-one__container">
                    <i class="fa fa-microphone mic-icon"></i>
                    <div class="feature-one__list">
                        <h3>Voice Recognition</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Incidunt deserunt </p>
                    </div>
                </div>
                <div class="feature-two__container">
                    <i class="fa fa-mobile mobile-icon"></i>
                    <div class="feature-two__list">
                        <h3>Connect with your phone</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, con sectetur adipisicing elit. Mauris</p>
                    </div>
                </div>
                <div class="feature-three__container">
                    <i class="fa fa-rss wifi-icon"></i>
                    <div class="feature-three__list">
                        <h3>Any task at hand</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, con sectetur adipisicing elit. Mauris</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="second-features">
            <div class="second__lists">
                <h4 class="second__min-title">Now upgraded for</h4>
                <h4 class="second__title">Best<br>Experience</h4>
                <h2 class="seccond__plus-one">+ Increased storage</h2>
                <p class="second-paragraf-one">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur cupiditate</p>
                <h3 class="second-percentage-one">30%</h3>
                <hr class="horizontal__icon-1">
                <h2 class="second__plus-two">+ Battery life</h2>
                <p class="second-paragraf-two">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis nesciunt</p>
                <h3 class="second-percentage-two">80%</h3>
                <hr class="second__icon-2">
                <h2 class="second__plus-three">+ Display surface</h2>
                <p class="second-paragraf-three">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore rem</p>
                <h3 class="second-percentage-three">60%</h3>
                <hr class="second__icon-3">
            </div>
            <div class="second-image">
                <img src="images/three-watches.jpg">
            </div>
        </section>
        <section class="last-feature">
            <h2 class="last-features__min-title">Adjustable Straps</h2>
            <h1 class="last-features__main-title">Choose the best<br>color for your activity</h1>
            <div class="last-features__list">
                <div class="last-feature__article">
                    <img src="images/watch-rotate1.jpg">
                    <h2 class="article__up-title">Wrist Brand</h2>
                    <a href="#" class="article__title-1">
                        <h2 style="color: #99ffee;">Mint Green</h2>
                    </a>
                    <h2 class="article__price">$25.50</h2>
                </div>
                <div class="last-feature__article">
                    <img src="images/watch-rotate.jpg" alt="">
                    <h2 class="article__up-title">Wrist Brand</h2>
                    <a href="#" class="article__title-1">
                        <h2 style="color: #cea9ff;">Purple Berry</h2>
                    </a>
                    <h2 class="article__price">$25.50</h2>
                </div>
                <div class="last-feature__article">
                    <img src="images/watch-rotate2.jpg" alt="">
                    <h2 class="article__up-title">Wrist Brand</h2>
                    <a href="#" class="article__title-1">
                        <h2 style="color: #f3b560;">Golden Sunset</h2>
                    </a>
                    <h2 class="article__price">$25.50</h2>
                </div>
                <div class="btn__see-more">
                    <!-- <a href="#">SEE MORE</a> -->
                    <button class="button-see-more">SEE MORE</button>
                </div>
            </div>
        </section>
        <section class="reviews__container" id="reviews">
            <h2 class="reviews__title">What our<br>customers say</h2>
            <div class="reviews__customers">
                <div class="customers__image"></div>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing
                    elit. Minus at asperiores quia maxime quasi veritatis
                    laboriosam quae deserunt quos quibusdam atque cupiditate
                    Lorem ipsum dolor sit.
                </p>
                <h3 class="customer__name">Ella Jonson</h3>
                <i class="fa fa-quote-right icon-quote__right"></i>
            </div>
        </section>
        <section class="social-icons">
            <div>
                <i class="fa fa-youtube-play fa-2x "></i>
                <i class="fa fa-twitter fa-2x"></i>
                <i class="fa fa-facebook fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
            </div>
        </section>
    </main>
    <?php
    require "footer.php";
    ?>

    <script src="shared.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
<footer>
    <nav class="main-footer">
        <ul class="main-footer__left-block">
            <img src="images/Jevelin_logo_light.png">
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit Dolorum.</p>
            <div class="main-footer__left-icons">
                <i class="fab fa-cc-visa fa-lg"></i>
                <i class="fab fa-cc-mastercard fa-lg"></i>
                <i class="fab fa-cc-amex fa-lg"></i>
                <i class="fab fa-cc-paypal fa-lg"></i>
                <i class="fab fa-cc-jcb fa-lg"></i>
            </div>
            <li><i class="fa fa-lock lock-icon"></i> Secure online payment</li>
        </ul>
        <ul class="main-footer__middle-block">
            <h1>Best Experience</h1>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Shipment</a></li>
            <li><a href="#">Returns</a></li>
            <li><a href="#">Gift Carts</a></li>
            <li><a href="#">Policies</a></li>
        </ul>
        <ul class="main-footer__right-block">
            <h1>Company</h2>
                <li><a href="#">About us</a></li>
                <li><a href="#">Press</a></li>
                <li><a href="#">Offices</a></li>
                <li><a href="#">Affiliates</a></li>
                <li><a href="#">Giveaway</a></li>
        </ul>
    </nav>
    <nav class="lowest-footer">
        <div class="lowest-footer__paragraf">
            <p>Copyright &copy; Jevelin 2018 </p>
        </div>
    </nav>
</footer>
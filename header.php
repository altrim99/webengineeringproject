<div class="backdrop"></div>
<header class="main-header">
    <div>
        <a href="index.php" class="main-header__logo">
            <img src="images/Jevelin_logo_dark.png" alt="Jevelin - your online shop for apple watch">
        </a>
    </div>
    <nav class="main-nav">
        <ul class="main-nav__items">
            <li class="main-nav__item">
                <a href="index.php">Home</a>
            </li>
            <li class="main-nav__item">
                <a href="shop.php">Shop</a>
            </li>
            <li class="main-nav__item">
                <a href="#reviews">Reviews</a>
            </li>
            <li class="main-nav__item">
                <a href="pages/aboutus.php">About Us</a>
            </li>
            <li class="main-nav__item">
                <a href="contact.php">Contact</a>
            </li>
        </ul>
    </nav>
    <button class="toggle-button">
        <span class="toggle-button__bar"></span>
        <span class="toggle-button__bar"></span>
        <span class="toggle-button__bar"></span>
    </button>
    <nav class="right-nav">
        <ul class="right-nav__items">
            <li class="right-nav__item">
                <?php
                if (isset($_SESSION['userNameId'])) {
                    echo '<p style="display: inline;">Welcome: ' . $_SESSION['userNameId'] . '</p>
                    <a href="view-cart.php" id="cart-icon-1" class="cart-icon">
                        <i class="fa fa-shopping-cart fa-lg"><span id="cart-count" class="badge">0</span></i>
                        </a>
                    <form class="logout-style" style="margin: 0; padding: 0; display: inline;" action="logout.php" method="post">
                        <button style="border: none;
                        background: #6394F8;
                        color: white;
                        cursor: pointer;
                        width: 80px;
                        height: 25px;
                        border-radius: 15px; "type="submit" name="logout-submit">Logout</button>
                    </form>';
                } else {
                    echo '<a href="login.php" id="login" class="login-style">Log in</a>
                    |
                    <a href="register.php" id="signup" class="signup-style">Sign up</a>';
                }
                ?>
            </li>
        </ul>
    </nav>
</header>
<nav class="mobile-nav">
    <ul class="mobile-nav__items">
        <?php
        if (isset($_SESSION['userNameId'])) {
            echo '<li class="mobile-nav__item">
            <a href="index.php">Home</a>
        </li>
        <li class="mobile-nav__item">
            <a href="shop.php">Shop</a>
        </li>
        <li class="mobile-nav__item">
            <a href="#">Reviews</a>
        </li>
        <li class="mobile-nav__item">
            <a href="pages/aboutus.php">About Us</a>
        </li>
        <li class="mobile-nav__item">
            <a href="contact.php">Contact</a>
        </li>
        <li class="mobile-nav__item">
            <a href="view-cart.php">Cart</a>
        </li>
        <form class="logout-style" style="margin: 0; padding: 0; display: inline;" action="logout.php" method="post">
            <button style="border: none;
            background: #6394F8;
            color: white;
            cursor: pointer;
            width: 80px;
            height: 25px;
            border-radius: 15px; "type="submit" name="logout-submit">Logout</button>
        </form>';
        } else {
            echo '<li class="mobile-nav__item">
            <a href="index.php">Home</a>
        </li>
        <li class="mobile-nav__item">
            <a href="shop.php">Shop</a>
        </li>
        <li class="mobile-nav__item">
            <a href="#">Reviews</a>
        </li>
        <li class="mobile-nav__item">
            <a href="pages/aboutus.php">About Us</a>
        </li>
        <li class="mobile-nav__item">
            <a href="contact.php">Contact</a>
        </li>
        <li class="mobile-nav__item">
            <a href="login.php">Log in</a>
        </li>
        <li class="mobile-nav__item">
            <a href="register.php">Sign Up</a>
        </li>';
        }
        ?>

    </ul>
</nav>
<?php 

require 'controllers/AuthController.php';
$user = new AuthController;

if (isset($_POST['login-submit'])) {
    $user->login($_POST);
}

session_start(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jevelin | Login</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shared.css" type="text/css">
    <link rel="stylesheet" href="css/login.css" type="text/css">
</head>
<body>
    <?php 
    require "header.php";
    ?>
    <main>
        <section>
            <section class="section-one">
                <article>
                    <h2>Welcome Back</h2>
                    <div>
                        <a href="index.html">Home</a>
                        <span class="after-icon">></span>
                        <span class="login-span">Login</span>
                    </div>
                </article>
            </section>
        </section>
        <section class="main__section">
            <h1 class="login-title">Account <span style="color: #7d1eff;">Login</span></h1>
            <?php 
            if (isset($_GET['error'])) {
                if($_GET['error'] == 'emptyfields'){
                    echo '<p id="error-login">Please fill in all fields!</p>';
                }
            }
            ?>
            <form class="login-form" onsubmit="return validateLogin(this)" method="post">
                <input type="text" id="username" name="username" placeholder="Username">
                <input type="password" id="password"  name="password" placeholder="Password">
                <label for="remember-me" class="remember-me">
                    <input type="checkbox" id="remember-me" name="remember-me">
                    Remember me
                </label>
                <a href="#" class="forgot-pass">Forgot Password?</a>
                <button id="button-login" type="submit" value="submit" name="login-submit">Login</button>
            </form>
        </section>
    </main>
    <?php 
    require "footer.php";
    ?>
    <script src="shared.js"></script>
    <!-- <script src="js/login.js"></script> -->
</body>
</html>